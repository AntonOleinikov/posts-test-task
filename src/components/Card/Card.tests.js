import React from 'react';
import { mount } from 'enzyme';
import CardComponent from './index';

describe(`<Card />`, () => {
  let wrapper = null;
  beforeAll(() => {
    wrapper = mount(
      <CardComponent
        title="test-title"
        body="test-body"
        userId="test-user-id"
        id="test-id"
      />
    );
  });

  test(`Mounts`, () => {
    expect(wrapper.debug().length).toBeGreaterThan(0);
  });

  test(`Contains props`, () => {
    expect(wrapper.contains('test-user-id')).toBeTruthy();
    expect(wrapper.contains('test-title')).toBeTruthy();
    expect(wrapper.contains('test-body')).toBeTruthy();
    expect(wrapper.contains('test-id')).toBeTruthy();
  });
});
