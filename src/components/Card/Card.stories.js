import React from 'react';
import Card from './index';

export default {
  component: Card,
  title: 'Components/Cards',
};

const Template = (args) => {
  return (
    <Card {...args} />
  )
};

export const Primary = Template.bind({});

Primary.args = {
  id: '1',
  userId: '2',
  title: 'Why do we use it?',
  body: `It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.`
};
