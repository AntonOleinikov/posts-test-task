import { PostCardContainer, PostCardTitle, PostCardBody, PostCardAdditionalInfo } from './PostCard.styles';

const PostCard = ({id, userId, body, title}) => {
  return (
    <PostCardContainer>
      <div>
        <PostCardTitle>
          {title}
        </PostCardTitle>
        <PostCardBody>
          {body}
        </PostCardBody>
      </div>

      <PostCardAdditionalInfo>
        <span>
          Id: <strong>{id}</strong>
        </span>
        <span>
          User Id: <strong>{userId}</strong>
        </span>
      </PostCardAdditionalInfo>
    </PostCardContainer>
  )
}

export default PostCard;