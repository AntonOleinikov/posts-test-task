import styled from 'styled-components';

export const PostCardContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  background-color: #ffffff;
  box-shadow: 4px 4px 15px rgb(0 0 0 / 5%);
  border-radius: 5px;
  padding: 10px;
  min-height: 180px;
`;

export const PostCardTitle = styled.h4``;

export const PostCardBody = styled.p``;

export const PostCardAdditionalInfo = styled.div`
  text-align: right;
  span {
    margin: 0 5px;
  };
`;