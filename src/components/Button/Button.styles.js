import styled from 'styled-components';

export const ButtonContainer = styled.button`
  border: none;
  cursor: pointer;
  padding: 10px 25px;
  font-size: 16px;
  background-color: #212121;
  color: #ffffff;
  border-radius: 3px;
  
  &:disabled {
    background-color: #9e9e9e;
    cursor: not-allowed;
  }
`;