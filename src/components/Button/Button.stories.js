import React from 'react';
import Button from './index';

export default {
  component: Button,
  title: 'Components/Buttons',
};

const Template = (args) => {
  return (
    <Button {...args}>
      Button
    </Button>
  )
};

export const Primary = Template.bind({});

Primary.args = {
  disabled: false
};
