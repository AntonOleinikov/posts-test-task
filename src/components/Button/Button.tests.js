import React from 'react';
import { mount } from 'enzyme';
import 'jest-styled-components';

import ButtonComponent from './index';

describe(`<Button />`, () => {
  let wrapper = null;
  beforeAll(() => {
    wrapper = mount(
      <ButtonComponent
        id="test-button-component"
      >Button value</ButtonComponent>
    );
  });

  test(`Mounts`, () => {
    expect(wrapper.debug().length).toBeGreaterThan(0);
  });

  test(`Contains props`, () => {
    expect(wrapper.contains('Button value')).toBeTruthy();
  });
});
