import { LoaderContainer } from './Loader.styles';
import Spinner from '../../common/images/loader.svg';

const Loader = ({ ...props }) => {
  return (
    <LoaderContainer
      {...{
        ...props,
      }}
    >
      <img
        src={Spinner}
        alt="Loading"
      />
    </LoaderContainer>
  )
}

export default Loader;