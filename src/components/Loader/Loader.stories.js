import React from 'react';
import Loader from './index';

export default {
  component: Loader,
  title: 'Components/Loaders',
};

const Template = (args) => {
  return (
    <Loader {...args} />
  )
};

export const Primary = Template.bind({});

Primary.args = {};
