import React from 'react';
import TextArea from './index';

export default {
  component: TextArea,
  title: 'Components/TextAreas',
};

const Template = (args) => {
  return (
    <div
      style={{
        maxWidth: 500
      }}
    >
      <TextArea {...args} />
    </div>
  )
};

export const Primary = Template.bind({});

Primary.args = {
  label: 'What is Lorem Ipsum?',
  value: 'Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.',
  error: '',
  disabled: false
};
