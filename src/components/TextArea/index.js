import React, { forwardRef } from 'react';
import { TextAreaElement, TextAreaContainer, Label, ErrorMessage } from './TextArea.styles';

const TextArea = forwardRef(({id, label, error, onChange, ...props}, ref) => {
  return (
    <TextAreaContainer>
      {label &&
      <Label
        error={error}
        htmlFor={id}
      >
        {label}
      </Label>
      }

      <TextAreaElement
        {...{
          error,
          id,
          ...props
        }}
        ref={ref}
        onChange={(event) => {
          onChange?.(event)
        }}
      />

      {error &&
      <ErrorMessage>
        {error}
      </ErrorMessage>
      }
    </TextAreaContainer>
  );
});

export default TextArea;
