import styled from 'styled-components';

export const TextAreaContainer = styled.div``;

export const TextAreaElement = styled.textarea`
  min-height: 160px;
  border: 1px solid #eeeeee;
  padding: 10px 12px;
  font-size: 16px;
  border-radius: 3px;
  width: 100%;
  ${({ error }) => {
    return error && `
      color: #e53935;
      border: 1px solid #e53935;
      background-color: #fce4ec;
      &:focus-visible {
        border: 2px solid #e53935;
        outline: none;
      }
    `
  }};
`;

export const Label = styled.label`
  display: block;
  margin: 5px 0 7px 0;
`;

export const ErrorMessage = styled.p`
  color: #e53935;
`;