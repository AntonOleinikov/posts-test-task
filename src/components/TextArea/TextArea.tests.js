import React from 'react';
import { mount } from 'enzyme';
import TextAreaComponent from './index';

describe(`<TextArea />`, () => {
  let wrapper = null;
  beforeAll(() => {
    wrapper = mount(
      <TextAreaComponent
        value="textarea-value"
        label="textarea-label"
        error="textarea-error"
      />
    );
  });

  test(`Mounts`, () => {
    expect(wrapper.debug().length).toBeGreaterThan(0);
  });

  test(`Contains props`, () => {
    expect(wrapper.contains('textarea-label')).toBeTruthy();
    expect(wrapper.contains('textarea-error')).toBeTruthy();
  });
});
