import React, { forwardRef } from 'react';
import { Input, InputContainer, Label, ErrorMessage } from './TextField.styles';

const TextField = forwardRef(({id, label, error, onChange, ...props}, ref) => {
  return (
    <InputContainer>
      {label &&
      <Label
        error={error}
        htmlFor={id}
      >
        {label}
      </Label>
      }

      <Input
        {...{
          error,
          id,
          ...props
        }}
        ref={ref}
        onChange={(event) => {
          onChange?.(event)
        }}
      />

      {error &&
      <ErrorMessage>
        {error}
      </ErrorMessage>
      }
    </InputContainer>
  );
});

export default TextField;
