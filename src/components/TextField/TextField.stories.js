import React from 'react';
import TextField from './index';

export default {
  component: TextField,
  title: 'Components/TextFields',
};

const Template = (args) => {
  return (
    <div
      style={{
        maxWidth: 500
      }}
    >
      <TextField {...args} />
    </div>
  )
};

export const Primary = Template.bind({});

Primary.args = {
  label: 'Where can I get some?',
  value: 'Lorem Ipsum',
  error: '',
  disabled: false,
};
