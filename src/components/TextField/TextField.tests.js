import React from 'react';
import { mount } from 'enzyme';
import TextFieldComponent from './index';

describe(`<TextField />`, () => {
  let wrapper = null;
  beforeAll(() => {
    wrapper = mount(
      <TextFieldComponent
        value="textfield-value"
        label="textfield-label"
        error="textfield-error"
      />
    );
  });

  test(`Mounts`, () => {
    expect(wrapper.debug().length).toBeGreaterThan(0);
  });

  test(`Contains props`, () => {
    expect(wrapper.contains('textfield-label')).toBeTruthy();
    expect(wrapper.contains('textfield-error')).toBeTruthy();
  });
});
