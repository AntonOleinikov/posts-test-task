import styled from 'styled-components';

export const HeaderContainer = styled.div`
  background-color: #212121;
`;

export const Navigation = styled.nav`
  a {
    margin: 0 10px;
    color: #ffffff;
  }
`;

export const Logo = styled.p`
  font-size: 40px;
  color: #ffffff;
  font-weight: 500;
  margin-right: 80px;
`;

// export const Logo = styled.img`
//   max-width: 120px;
//   margin-right: 20px;
// `;