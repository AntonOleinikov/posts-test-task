import { HeaderContainer, Logo, Navigation } from './Header.styles';
import { Container, FlexRowCenter } from '../../common/styles/globals';
import {
  Link
} from 'react-router-dom';

const Header = ({ menu }) => {
  return (
    <HeaderContainer>
      <Container>
        <FlexRowCenter>
          <Link to="/posts/list">
            <Logo>TASK</Logo>
          </Link>
          <Navigation>
            <Link to="/posts/list">Posts list</Link>
            <Link to="/posts/create">Create new post</Link>
          </Navigation>
        </FlexRowCenter>
      </Container>
    </HeaderContainer>
  )
}

export default Header;