import api from '../axios';

export const makeGetAllPosts = () => {
  return api.get('/posts');
}

export const makeCreateNewPost = (data) => {
  return api.post('/posts', data);
}