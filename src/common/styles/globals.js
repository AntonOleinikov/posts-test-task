import styled from 'styled-components';

export const Container = styled.div`
  max-width: 1200px;
  padding: 15px;
  margin: 0 auto;

  .p0 {
    padding: 0;
  }
  
  .pl0 {
    padding-left: 0;
  }

  .pr0 {
    padding-right: 0;
  }

  .pb0 {
    padding-bottom: 0;
  }

  .pt0 {
    padding-top: 0;
  }
`;

export const FlexRowCenter = styled.div`
  display: flex;
  align-items: center;
`;