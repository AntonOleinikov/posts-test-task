import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import { Header } from './components';

import PostsList from './screens/PostsList';
import PostsCreation from './screens/PostCreation';

function App() {
  return (
    <Router>
      <Header/>
      <div>
        <Switch>
          <Route exact path="/">
            <Redirect to="/posts/list" />
          </Route>
          <Route path="/posts/list">
            <PostsList />
          </Route>
          <Route path="/posts/create">
            <PostsCreation />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
