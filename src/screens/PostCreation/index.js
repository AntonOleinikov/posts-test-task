import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Container } from '../../common/styles/globals';
import { TextField, TextArea, Button, Loader } from '../../components';
import { AddForm, AddFormContainer } from './PostCreation.styles';
import { makeCreateNewPost } from '../../backend/methods';

const PostsCreation = () => {
  const [loading, setLoading] = useState(false);
  const {
    register,
    reset,
    handleSubmit,
    formState: {
      errors,
      isDirty,
      isValid
    }
  } = useForm({ mode: 'onChange' });

  const onSubmit = (data) => {
    if (!isValid) return;

    setLoading(true);

    makeCreateNewPost(data)
      .then(() => {
        reset();
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      })
  };
  return (
    <Container>
      <h1>Create new post</h1>

      <AddFormContainer>
        <AddForm>
          <TextField
            error={errors?.title?.message}
            label="Title"
            {
              ...register("title", {
                required: 'Body cannot be empty',
                minLength: {
                  value: 3,
                  message: "Min title length is 3 symbols."
                },
                maxLength: {
                  value: 30,
                  message: "Max title length is 3 symbols."
                }
              })
            }
            style={{
              marginBottom: 5
            }}
          />

          <TextArea
            label="Body"
            error={errors?.body?.message}
            {
              ...register("body", {
                required: 'Body cannot be empty',
                minLength: {
                  value: 3,
                  message: "Min body length is 3 symbols."
                },
                maxLength: {
                  value: 260,
                  message: "Max body length is 260 symbols."
                }
              })
            }
            style={{
              marginBottom: 5
            }}
          />

        </AddForm>

        <Button
          style={{
            marginTop: 15
          }}
          disabled={!isDirty}
          onClick={() => {
            handleSubmit(onSubmit)()
          }}
        >
          Add new post
        </Button>
      </AddFormContainer>

      {
        loading && <Loader />
      }
    </Container>
  )
}

export default PostsCreation;