import styled from 'styled-components';

export const Title = styled.h1`
`;

export const AddFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

export const AddForm = styled.form`
  width: 100%;
`;
