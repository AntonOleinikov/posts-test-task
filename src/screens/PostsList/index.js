import {useEffect, useState} from 'react';
import {makeGetAllPosts} from '../../backend/methods';
import {Container} from '../../common/styles/globals';
import {Title, PostCardItem, Content} from './PostsList.styles';
import { Card, Loader } from '../../components';

const PostsList = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    makeGetAllPosts()
      .then(({data}) => {
        setPosts(data);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      })
  }, []);

  return (
    <Container>
      <Title>Posts</Title>
      <Content>
        {
          (posts && posts.length) && !loading ? (
            <>
              {
                posts.map(({id, title, body, userId}) => (
                  <PostCardItem
                    key={id}
                  >
                    <Card
                      {...{
                        id,
                        body,
                        title,
                        userId
                      }}
                    />
                  </PostCardItem>
                ))
              }
            </>
          ) : null
        }

        {
          (!posts || !posts.length) && !loading ? (
            <h3>
              No posts found
            </h3>
          ) : null
        }

        {
          loading ? (
            <Loader />
          ) : null
        }
      </Content>
    </Container>
  )
}

export default PostsList;