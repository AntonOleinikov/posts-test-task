import styled from 'styled-components';

export const Title = styled.h1`
`;

export const PostCardItem = styled.div`
  flex: 1 0 35%;
  margin: 10px;
`;

export const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
`;